const sum = (a: number, b: number) : number => { return a + b }

describe('Test operations', () => {
    test ('1 + 1', () => {
        expect(sum(1, 1)).toBe(2);
    })
})